package com.ahmedhamdy.testinddownload.utils

object AppConstants {


    const val ENGLISH = "en"
    const val ARABIC = "ar"

    const val ARG_TYPE = "ARG_TYPE"
    const val ARG_ID = "ARG_ID"

    const val QUIZ_DISPLAY_DATE_FORMAT = "E MMM dd yyyy HH:mm ZZZZ"
    const val QUIZ_SERVER_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss ZZZZ"
    const val QUIZ_TIME_FORMAT = "HH:mm"

    const val QUIZ_STATUS_FAILED = "failed"
    const val QUIZ_STATUS_SUCCESS = "passed"
    const val QUIZ_STATUS_ONGOING = "ongoing"

    const val DYNAMIC_TYPE_INSTRUCTOR = "instructor"

    /**
     * some results code
     */

    const val ITEM_SAVED = 5
    const val ITEM_UNSAVED = 6

    /**
     * items actions
     */
    const val ITEM_CLICKED = 1
    const val CLICKED_SAVE = 2
    const val CLICKED_MORE = 3
    const val CLICKED_COURSE = 4
    const val CLICKED_TEACHER = 5
    const val CLICKED_TOPIC = 6

    const val NOT_PIECTURE = -1
    const val PLAYBACK = 1
    const val VIDEO_QUALITY = 2

    const val APP = "app"
    const val DEV = "dev"
    const val USB = "usb"
    const val EMULATOR = "emulator"
    const val OUT_PLAY = "out_play"

    const val FULL_TRUE = 1
    const val FULL_FALSE = 2
    const val HIDE_NAV_UI = 3

    /**
     * P u b l i c_s t a r t
     */
    const val PUBLIC_START = """-----BEGIN PUBLIC KEY-----"""
    const val PUBLIC_END = "-----END PUBLIC KEY-----"


    const val RECORD_PERMISSION = 100
    const val TYPE_BEGINNER = "beginner"
    const val TYPE_INTERMIDATE = "intermediate"

    const val USER = "USER"
    const val FCM_TOKEN = "FCM_TOKEN"
    const val USER_TYPE = "USER_TYPE"
    const val DEVICEID_TOKEN = "DEVICEID_TOKEN"
    const val LANGUAGE = "LANGUAGE"
    const val DEVICE_LANGUAGE = "D_LANGUAGE"
    const val FIREBASE_TOKEN = "FIREBASE_TOKEN"
    const val EMAIL = "email"
    const val DEFAULT_EMAIL = "GUEST"


    const val SUCCESS_APPLE = 0
    const val FAIL_APPLE = 1


    const val UPDATE_WATERMARK = 10

    /**
     * Choices types
     */
    const val CHOICE_TEXT = 0
    const val CHOICE_IMAGE = 1
    const val CHOICE_MIX = 2

    const val TYPE_STUDENT = "Student"


    /**
     * Quiz question actions
     */

    const val CHOICE_IMAGE_CLICKED = 0
    const val CHOICE_SELECTED = 1
    const val QUESTION_IMAGE_CLICKED = 2
    const val CLEAR_ANSWER_CLICKED = 3


    /**
     *
     */
    const val PREF_IS_SIGNED = "signed"
    const val PREF_ACCESS_TOKEN = "accessToken"
    const val PREF_IS_SOCIAL = "social"
    const val MY_PREFERENCES = "app_pref"
    const val enable_categorization = "enable_categorization"
    const val PREF_CLASS_ID = "PREF_CLASS_ID"


    val IMAGE_URL: String = "URL"
    const val SEEK_TIME_PERIOD = 10

    const val SPLASH_TIME = 1000L

    /**
     * course detail intent keys
     */
    const val COURSE_ID = "course_id"
    const val COURSE_OJECT = "course_obj"
    const val REVIEW_OBJECT = "review_obj"
    const val TRACK_OJECT = "track_obj"
    const val EVALUATION_OJECT = "EVA_obj"

    const val POSITION_TAB = "POSITION_TAB"

    /**
     * CourseDetail
     */
    const val DETAIL_BOTTOM_RESOURCES = 0
    const val DETAIL_BOTTOM_ASSIGNMENTS = 1
    const val DETAIL_BOTTOM_QUIZZES = 2
    const val DETAIL_BOTTOM_MY_EVALUATION = 3

    /**
     * course detail bus values
     */
    const val DETAIL_ABOUT_TAB = 10
    const val DETAIL_INSTRUCTOR_TAB = 11
    const val DETAIL_REVIEWS_TAB = 12
    const val DETAIL_CONTENT_TAB = 13

    /**
     * session states
     */

    const val SESSION_TODO = "todo"
    const val SESSION_COMPLETED = "completed"

    /**
     * SESSION detail intent keys
     */
    const val SESSION_OBJ = "session_obj"
    const val ASSIGNEMENT_ID = "assid"

    /**
     * course modes
     */
    const val MODE_GUEST = 1
    const val MODE_JOINED = 2

    const val COURSE_TYPE_ONGOING = 1
    const val COURSE_TYPE_ONLINE = 2


    const val BUS_ACTION_PAUSE_VIDEO = 1
    const val BUS_ACTION_PLAY_VIDEO = 2
    const val BUS_ACTION_TRY_AGAIN = 3
    const val BUS_ACTION_SHOW_LOADING = 4
    const val BUS_ACTION_HIDE_CONTINUE_LEARNING = 5
    const val BUS_ACTION_LOAD_CONTENT_AGAIN = 6
    const val BUS_ACTION_REFRESH_HOME = 7
    const val BUS_ACTION_REFRESH_COURSES = 8
    const val BUS_ACTION_OPEN_MORE_COURSES = 9
    const val BUS_ACTION_CLICKED_BACK = 10
    const val BUS_ACTION_FULL_SCREEN_CLOSED = 15
    const val BUS_ACTION_CHECK_SOUND = 16
    const val BUS_ACTION_CLEAR_PADDING_ABOUT = 17
    const val BUS_ACTION_CLEAR_BLOCK = 18
    const val BUS_ACTION_BLOCK_ADDED = 19
    const val BUS_ACTION_REFRESH_CONTENT = 20
    const val BUS_ACTION_REFRESH_HOME1 = 21

    const val OPEN_SIGN_IN = 0
    const val OPEN_SIGN_UP = 1

    const val PROFILE_URL1 =
        "https://media-exp1.licdn.com/dms/image/C4D03AQHgmwt_zsTiwg/profile-displayphoto-shrink_200_200/0?e=1605139200&v=beta&t=dk_Z8J27YVRq7WQyepiDpV1qooH-2SAe0djh7LZZo94"
    const val PROFILE_URL2 =
        "https://scontent-hbe1-1.xx.fbcdn.net/v/t1.0-9/43130986_850236535181331_7713443775338512384_n.jpg?_nc_cat=107&_nc_sid=09cbfe&_nc_ohc=HL7bBik3QOoAX9O5c4v&_nc_ht=scontent-hbe1-1.xx&oh=bbe88131dcfc3cbfe15c190648ef86f1&oe=5F7A4810"
    const val COMPANY_LINK = "https://inovaeg.com/"


    /**
     * more screen items
     */
    const val MORE_PROFILE = 0
    const val MORE_MY_LEARNING = 1
    const val MORE_MANAGED_COURSES = 2
    const val MORE_SAVED_LIST = 3
    const val MORE_BLOG = 4
    const val MORE_EVENTS = 5
    const val MORE_SETTINGS = 6
    const val MORE_ORDERS = 7
    const val MORE_CART = 8


    /**
     * oPTIONS
     */
    const val OPTION_UPLOAD_MATERIALS = 0
    const val OPTION_UPLOAD_ASSIGNMENTS = 1
    const val OPTION_EVALUATE_STUDENTS = 2
    const val OPTION_PREVIEW_COURSE = 3


    /**
     * Home actions
     */
    const val HOME_UPDATE_PROFILE = 0
    const val ACCOUNT_FRAGMENT_UPDATE = 1001

    /**
     * settings screen items
     */
    const val SETTING_ABOUT = 0
    const val SETTING_CHANGE_PASS = 1
    const val SETTING_HELP = 2
    const val SETTING_PRIVACY_POLICY = 3
    const val SETTING_TERMS = 4
    const val SETTING_LOGOUT = 5
    const val SETTING_LANGUAGE = 6

    /**
     * Actios course
     */
    const val COURSE_REVIEW_CLICKED = 0
    const val COURSE_ADD_REVIEW_CLICKED = 1


    /**
     * File picker options
     */
    const val IMAGE_CAMERA = 0
    const val IMAGE_GALLERY = 1
    const val DOCUMENT = 2
    const val VIDEO = 3
    const val VOICE_MEMO = 4
    const val DELETE_PHOTO = 5

    /**
     * File picker request codes
     */
    const val RC_IMAGE_CAMERA = 10
    const val RC_IMAGE_GALLERY = 11
    const val RC_DOCUMENT = 12
    const val RC_VIDEO = 13
    const val RC_AUDIO = 14


    /**
     * dummy
     */

    const val NO_INTERNET = "No Internet Connection"
    const val SOME_WRONG = "Something went wrong"
    const val UNAUTHORIZED_REQUEST = "UNAUTHORIZED_REQUEST"
    const val TIME_OUT = "TIME_OUT"
    const val MISS_SCHOOL_ERROR = "MISS_SCHOOL_ERROR"
    const val NOTALLOWED_REQUEST_TYPE = "NOTALLOWED_REQUEST_TYPE"


    const val CONSTANT_WHEN_BACK = 111


    const val INTENT_ARG_SHOW = "show"


    const val APLLICATION_OVER = "TYPE_APPLICATION_OVERLAY"
}