package com.ahmedhamdy.testinddownload.views


import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log

import androidx.appcompat.app.AppCompatActivity
import com.ahmedhamdy.testinddownload.R
import com.ahmedhamdy.testinddownload.utils.Utils
import com.google.android.material.snackbar.Snackbar
import com.tonyodev.fetch2core.*
import kotlinx.android.synthetic.main.activity_single_download.*
import com.ahmedhamdy.testinddownload.ExampleService
import com.ahmedhamdy.testinddownload.utils.App
import com.ahmedhamdy.testinddownload.utils.App.Companion.fetch
import com.ahmedhamdy.testinddownload.utils.App.Companion.request
import com.ahmedhamdy.testinddownload.utils.SharedPreferencesManager
import com.tonyodev.fetch2.*



private const val TAG = "SingleDownloadActivity"

class SingleDownloadActivity: AppCompatActivity(), FetchObserver<Download> {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_download)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if(!SharedPreferencesManager.getBoolean(Utils.URL)){
                enqueueDownload()
                SharedPreferencesManager.putBoolean(Utils.URL)
                startForegroundService(Intent(this, ExampleService::class.java))
            }else{
                fetch.attachFetchObserversForDownload(request.id, this)
            }
        }else{
            startService(Intent(this, ExampleService::class.java))
        }

    }


    private fun stopService() {
        val serviceIntent = Intent(this, ExampleService::class.java)
        stopService(serviceIntent)
    }

    override fun onChanged(data: Download, reason: Reason) {
        //customNotification.postDownloadUpdate(data)
        updateViews(data, reason)
    }


    private fun enqueueDownload() {
        val url = Utils.URL
        val filePath = Utils.getRootDirPath(this) + "/" + Utils.getNameFromUrl(url)
        App.request = Request(url, filePath)

        fetch.attachFetchObserversForDownload(App.request.id, this)
            .enqueue(App.request,
                { result -> App.request = result }
            ) { result -> Log.d(TAG, result.toString()) }
    }

    private fun updateViews(download: Download, reason: Reason) {
        if (App.request.id == download.id) {
            if (reason == Reason.DOWNLOAD_QUEUED || reason == Reason.DOWNLOAD_COMPLETED) {
                setTitleView(download.file)
            }
            cancelButton.setOnClickListener {
                //fetch.cancel(download.id)
                fetch.remove(download.id)
            }
            setProgressView(download)
            etaTextView.text = Utils.getETAString(this, download.etaInMilliSeconds)
            downloadSpeedTextView.text = Utils.getDownloadSpeedString(
                this,
                download.downloadedBytesPerSecond
            )
            if (download.error != Error.NONE) {
                showDownloadErrorSnackBar(download.error)
            }
        }
    }
    private fun setTitleView( fileName: String) {
        val uri = Uri.parse(fileName)
        titleTextView.text = uri.lastPathSegment
    }

    private fun setProgressView(download: Download) {
        when (download.status) {
            Status.QUEUED -> {
                progressTextView.setText(R.string.queued)
                startPauseResumeButton.setText(R.string.pause)
                startPauseResumeButton.setOnClickListener {
                    fetch.pause(download.id)
                }
            }
            Status.PAUSED ->{
                startPauseResumeButton.setText(R.string.resume)
                startPauseResumeButton.setOnClickListener {
                    fetch.resume(download.id)
                }
            }
            Status.FAILED ->{
                startPauseResumeButton.setText(R.string.retry)
                startPauseResumeButton.setOnClickListener {
                    fetch.retry(download.id)
                }
            }
            Status.ADDED -> {
                progressTextView.setText(R.string.added)
                //showNotification(download.id)
                /*startPauseResumeButton.setText(R.string.start)
                startPauseResumeButton.setOnClickListener {
                    fetch.resume(download.id)
                }*/
            }
            Status.DOWNLOADING, Status.COMPLETED -> {
                startPauseResumeButton.setText(R.string.pause)
                startPauseResumeButton.setOnClickListener {
                    fetch.pause(download.id)
                }
                if (download.progress == -1) {
                    progressTextView.setText(R.string.downloading)
                } else {
                    val progressString = resources.getString(R.string.percent_progress, download.progress)
                    progressTextView.text = progressString
                }
                if(download.progress == 100) stopService()
                /*notificationBuilder?.let { builder ->
                    if(download.progress == 100) builder.setContentText("Download Finish")
                    builder.setProgress(100, download.progress, false)
                    notificationManagerCompat?.let { it.notify(download.id, builder.build()) }
                }*/
            }
            else -> {
                progressTextView.setText(R.string.status_unknown)
            }
        }
    }
    private fun showDownloadErrorSnackBar(error: Error) {
        val snackbar: Snackbar = Snackbar.make(
            activity_single_download,
            "Download Failed: ErrorCode: $error", Snackbar.LENGTH_INDEFINITE
        )
        snackbar.setAction(R.string.retry) { v ->
            fetch.retry(App.request.id)
            snackbar.dismiss()
        }
        snackbar.show()
    }

}
