package com.ahmedhamdy.testinddownload.utils

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import android.os.Environment
import android.webkit.MimeTypeMap
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import com.ahmedhamdy.testinddownload.R
import com.tonyodev.fetch2.Fetch
import com.tonyodev.fetch2.Priority
import com.tonyodev.fetch2.Request
import java.io.File
import java.io.IOException
import java.text.DecimalFormat
import java.util.ArrayList

object Utils {
    var request: Request? = null
    val fetchLiveData  = MutableLiveData<Request>()
    val URL = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"

    fun getNameFromUrl(url: String?): String? {
        return Uri.parse(url).lastPathSegment
    }


    fun getRootDirPath(context: Context): String{
        return if(Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()){
            val file = ContextCompat.getExternalFilesDirs(context.applicationContext, null)[0]
            file.absolutePath
        }else{
            context.applicationContext.filesDir.absolutePath
        }
    }

    fun getInternalDirPath(context: Context): String{
        val videosDir = File(context.filesDir, "savedVideosDir")
        if (!videosDir.exists()) {
            videosDir.mkdir()
        }
        return videosDir.path
    }

    fun getETAString(context: Context, etaInMilliSeconds: Long): String{
        if (etaInMilliSeconds < 0) {
            return ""
        }
        var seconds = (etaInMilliSeconds / 1000).toInt()
        val hours = (seconds / 3600).toLong()
        seconds -= (hours * 3600).toInt()
        val minutes = (seconds / 60).toLong()
        seconds -= (minutes * 60).toInt()
        return when {
            hours > 0 -> {
                context.getString(R.string.download_eta_hrs, hours, minutes, seconds)
            }
            minutes > 0 -> {
                context.getString(R.string.download_eta_min, minutes, seconds)
            }
            else -> {
                context.getString(R.string.download_eta_sec, seconds)
            }
        }
    }

    fun getDownloadSpeedString(context: Context, downloadedBytesPerSecond: Long): String{
        if (downloadedBytesPerSecond < 0) {
            return ""
        }
        val kb = downloadedBytesPerSecond.toDouble() / 1000.toDouble()
        val mb = kb / 1000.toDouble()
        val decimalFormat = DecimalFormat(".##")
        return when {
            mb >= 1 -> {
                context.getString(R.string.download_speed_mb, decimalFormat.format(mb))
            }
            kb >= 1 -> {
                context.getString(R.string.download_speed_kb, decimalFormat.format(kb))
            }
            else -> {
                context.getString(R.string.download_speed_bytes, downloadedBytesPerSecond)
            }
        }
    }

    fun getProgress(downloaded: Long, total: Long): Int{
        return when {
            total < 1 -> {
                -1
            }
            downloaded < 1 -> {
                0
            }
            downloaded >= total -> {
                100
            }
            else -> {
                ((downloaded.toDouble() / total.toDouble()) * 100).toInt()
            }
        }
    }
}