package com.ahmedhamdy.testinddownload.utils

import android.app.Application
import com.tonyodev.fetch2.*
import com.tonyodev.fetch2core.Downloader
import com.tonyodev.fetch2rx.RxFetch

class App: Application() {
    override fun onCreate() {
        super.onCreate()

        val fetchConfig = FetchConfiguration.Builder(this)
            .enableRetryOnNetworkGain(true)
            .setDownloadConcurrentLimit(3)
            .setHttpDownloader(HttpUrlConnectionDownloader(Downloader.FileDownloaderType.PARALLEL))
            .build()

        Fetch.Impl.setDefaultInstanceConfiguration(fetchConfig)
        RxFetch.Impl.setDefaultRxInstanceConfiguration(fetchConfig)
        fetch = Fetch.getDefaultInstance()

    }
    companion object{
        lateinit var INSTANCE: App
        lateinit var fetch: Fetch
        lateinit var request: Request

    }
    init {
        INSTANCE = this
    }

}