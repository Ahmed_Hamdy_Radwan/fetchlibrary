package com.ahmedhamdy.testinddownload

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ahmedhamdy.testinddownload.views.SingleDownloadActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        single.setOnClickListener {
            startActivity(Intent(this, SingleDownloadActivity::class.java))
        }
    }
}