package com.ahmedhamdy.testinddownload

import android.content.Intent
import android.os.IBinder

import android.util.Log

import com.ahmedhamdy.testinddownload.utils.App

import com.ahmedhamdy.testinddownload.utils.Utils
import com.ahmedhamdy.testinddownload.views.CustomNotification
import com.tonyodev.fetch2.*
import com.tonyodev.fetch2core.FetchObserver
import com.tonyodev.fetch2core.Reason

private const val TAG = "ExampleService"
class ExampleService: CustomNotification(App.INSTANCE.applicationContext), FetchObserver<Download> {


    private lateinit var fetch: Fetch
    private lateinit var request: Request

    override fun getFetchInstanceForNamespace(namespace: String): Fetch {
        return fetch
    }

    override fun onCreate() {
        fetch = App.fetch
        request = App.request
        fetch.attachFetchObserversForDownload(request.id, this)
            .enqueue(request,
                { result -> request = result }
            ) { result -> Log.d(TAG, result.toString()) }
        //enqueueDownload()
        super.onCreate()

    }
    private fun enqueueDownload() {
        val url = Utils.URL
        val filePath = Utils.getRootDirPath(App.INSTANCE.applicationContext) + "/" + Utils.getNameFromUrl(url)
        request = Request(url, filePath)
        //request.extras = getExtrasForRequest(request)
        fetch.attachFetchObserversForDownload(request.id, this)
            .enqueue(request,
                { result -> request = result }
            ) { result -> Log.d(TAG, result.toString()) }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        startForeground(1, getNotificationBuilder(request.id, request.groupId).build())
        return START_NOT_STICKY
    }
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onChanged(data: Download, reason: Reason) {
        when(data.status){
            Status.PAUSED ->{
                fetch.pause(data.id)
            }
        }
        postDownloadUpdate(data)
    }

    override fun onDestroy() {
        stopForeground(true)
        stopSelf()
        super.onDestroy()
    }

}

/*
private var notificationBuilder: Notification.Builder?= null
    private var builder: NotificationCompat.Builder? = null
    private var id: Int = 0
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate() {
        enqueueDownload()
        super.onCreate()
       // startForeground()
        startForeground2()

    }
    private fun startForeground2(){
        /*builder = customNotification.geDownloadNotificationsBuilderMap(id)
        builder?.let {
            startForeground(2, it.build())
        }*/
    }
    @RequiresApi(Build.VERSION_CODES.O)
    private fun startForeground(){
        notificationBuilder =  Notification.Builder(applicationContext, "CHANNEL_1")
        notificationBuilder?.let { builder ->
            builder.setContentText("Downloading File")
            builder.setSmallIcon(R.drawable.sym_def_app_icon)
            builder.setAutoCancel(false)
            builder.setProgress(100, 0, false)
            builder.setWhen(System.currentTimeMillis())
            builder.setPriority(Notification.PRIORITY_DEFAULT)
            // Show the notification
           // startForeground(2, builder.build())
           // startForeground(2, customNotification.getNotificationBuilder().build())


        }
    }

    override fun onChanged(data: Download, reason: Reason) {
        /*notificationBuilder?.let{
            it.setProgress(100, data.progress, false)
            startForeground(2, it.build())
        }*/
        customNotification.postDownloadUpdate(data)

        builder?.let {
            it.setProgress(100, data.progress, false)
            startForeground(1, it.build())
        }

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        //startForeground(2, customNotification.geDownloadNotificationsBuilderMap()?.build())
        return START_STICKY
    }
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    private fun enqueueDownload() {
        val url = Utils.URL
        val filePath = Utils.getRootDirPath(App.INSTANCE.applicationContext) + "/" + Utils.getNameFromUrl(url)
        var request = Request(url, filePath)
        //request.extras = getExtrasForRequest(request)
        fetch.attachFetchObserversForDownload(request.id, this)
            .enqueue(request,
                { result -> request = result }
            ) { result -> Log.d(TAG, result.toString()) }
        id = request.id
        builder = customNotification.geDownloadNotificationsBuilderMap(request.id)
        /*fetch.enqueue(request, object : Func<Request>{
            override fun call(result: Request) {

            }

        })*/
    }

    override fun onDestroy() {
        stopForeground(true)
        stopSelf()
        super.onDestroy()
    }
 */