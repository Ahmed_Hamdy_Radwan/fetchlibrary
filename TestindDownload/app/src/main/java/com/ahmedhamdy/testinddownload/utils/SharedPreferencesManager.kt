package com.ahmedhamdy.testinddownload.utils

import android.content.Context
import android.content.SharedPreferences




object SharedPreferencesManager {

    private val sharedPreferences: SharedPreferences by lazy {
        App.INSTANCE
            .getSharedPreferences(
                AppConstants.MY_PREFERENCES,
                Context.MODE_PRIVATE
            )
    }

    private val editor: SharedPreferences.Editor by lazy {
        sharedPreferences.edit()
    }


    fun putBoolean(videoUrl: String){
        editor.putBoolean(videoUrl, true).commit()
    }
    fun getBoolean(videoUrl: String): Boolean{
        return sharedPreferences.getBoolean(videoUrl, false)
    }


    fun removeDownloadedVideo(videoKey: String) {
        editor.remove(videoKey).commit()
    }





//    var user: AccountResponse?
//        get() = sharedPreferences.getString(AppConstants.USER, null)?.let {
//            Gson().fromJson(it, AccountResponse::class.java)
//        }
//        set(value) = editor.putString(AppConstants.USER,
//            Gson().toJson(value)).apply()

    fun setEmail(email: String) {
        editor.putString(AppConstants.EMAIL, email).apply()
    }

    fun getEmail() = sharedPreferences.getString(
        AppConstants.EMAIL,
        AppConstants.DEFAULT_EMAIL
    )

    fun saveDeviceUUID(deviceId: String?) {
        editor.putString(AppConstants.DEVICEID_TOKEN, deviceId).apply()
    }

    fun getDeviceUUID(): String? {
        return sharedPreferences.getString(
            AppConstants.DEVICEID_TOKEN,
            null
        )
    }

    fun deleteDeviceIdToken() {
        editor.remove(AppConstants.DEVICEID_TOKEN).apply()
    }

    fun saveLanguage(language: String?) {
        editor.putString(AppConstants.LANGUAGE, language).apply()
    }

    fun saveDeviceLanguage(language: String?) {
        editor.putString(AppConstants.DEVICE_LANGUAGE, language).apply()
    }

    fun getLanguage(): String {
        return sharedPreferences.getString(
            AppConstants.LANGUAGE,
            AppConstants.ENGLISH
        ) ?: AppConstants.ENGLISH
    }

    fun getDeviceLanguage(): String? {
        return sharedPreferences.getString(
            AppConstants.DEVICE_LANGUAGE,
            "en"
        )
    }


    fun saveFirebaseToken(firebaseToken: String?) {
        editor.putString(AppConstants.FIREBASE_TOKEN, firebaseToken)
            .apply()
    }

    fun getFirebaseToken(): String? {
        return sharedPreferences.getString(
            AppConstants.FIREBASE_TOKEN,
            null
        )
    }

    fun saveAccessToken(accessToken: String?) {
        editor.putString(AppConstants.PREF_ACCESS_TOKEN, accessToken)
            .apply()
    }

    fun getAccessToken(): String? {
        return sharedPreferences.getString(
            AppConstants.PREF_ACCESS_TOKEN,
            null
        )
    }

    private fun deleteAccessToken() {
        editor.remove(AppConstants.PREF_ACCESS_TOKEN).apply()
    }

    fun saveFCM(fcm: String?) {
        editor.putString(AppConstants.FCM_TOKEN, fcm).apply()
    }

    fun getFCM(): String? {
        return sharedPreferences.getString(AppConstants.FCM_TOKEN, null)
    }

    fun saveUserType(userType: String?) {
        editor.putString(AppConstants.USER_TYPE, userType).apply()
    }

    fun getUserType(): String? {
        return sharedPreferences.getString(AppConstants.USER_TYPE, null)
    }

    fun isSigned(): Boolean {
        return sharedPreferences.getBoolean(
            AppConstants.PREF_IS_SIGNED,
            false
        )
    }

    fun setSigned(state: Boolean) {
        editor.putBoolean(AppConstants.PREF_IS_SIGNED, state).apply()
    }

    fun isStudent(): Boolean {
        return AppConstants.TYPE_STUDENT == getUserType()
    }

    fun setSocilaUser(state: Boolean) {
        editor.putBoolean(AppConstants.PREF_IS_SOCIAL, state).apply()
    }

    fun isSocialUser(): Boolean {
        return sharedPreferences.getBoolean(
            AppConstants.PREF_IS_SOCIAL,
            false
        )
    }

    fun logoutConfigs() {
        /* deleteDeviceIdToken()
         deleteAccessToken()
         setSigned(false)*/
        deleteAccessToken()
        setSigned(false)
        // editor.clear().apply()

    }

    fun saveConfiguration(name: String, value: String) {
        editor.putString(name, value).apply()
    }

    fun isEnabledCategorization(): Boolean {
        return sharedPreferences.getBoolean(
            AppConstants.enable_categorization, false
        )
    }

    fun getClassId(): Int {
        return sharedPreferences.getInt(AppConstants.PREF_CLASS_ID, 1)
    }

    fun setClassId(id: Int) {
        editor.putInt(AppConstants.PREF_CLASS_ID, id).apply()
    }

}